package com.example.alejandro.matematicas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    // Declaracion de las variables
    private EditText num1, num2;
    private TextView resul;

    String n1 = "";
    String n2 = "";

    int numero1;
    int numero2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        num1 = (EditText)findViewById(R.id.txtNumero1);
        num2 = (EditText)findViewById(R.id.txtNumero2);
        resul = (TextView)findViewById(R.id.txtResultado);
    }



        public void Sumar(View add)
        {
             n1 = num1.getText().toString();
             n2 = num2.getText().toString();

             numero1 = Integer.parseInt(n1);
             numero2 = Integer.parseInt(n2);

             int suma = numero1 + numero2;
             String ressuma = String.valueOf(suma);
             resul.setText(ressuma);
        }

        public void Res(View supr)
        {
            n1 = num1.getText().toString();
            n2 = num2.getText().toString();

            numero1 = Integer.parseInt(n1);
            numero2 = Integer.parseInt(n2);

            int resta = numero1 - numero2;
            String resresta = String.valueOf(resta);
            resul.setText(resresta);
        }

        public void multiplicar(View prod)
        {
            n1 = num1.getText().toString();
            n2 = num2.getText().toString();

            numero1 = Integer.parseInt(n1);
            numero2 = Integer.parseInt(n2);

            int producto = numero1*numero2;
            String produc = String.valueOf(producto);
            resul.setText(produc);
        }

        //Metodo dividir
        public void dividir(View divid)
        {
            n1 = num1.getText().toString();
            n2 = num2.getText().toString();

            numero1 = Integer.parseInt(n1);
            numero2 = Integer.parseInt(n2);

            if (numero2 == 0)
            {
                Toast.makeText(this,"El dividendo no puede ser cero",Toast.LENGTH_SHORT).show();
            }else
            {
                double div = numero1/numero2;
                String respdiv = String.valueOf(div);
                resul.setText(respdiv);
            }
        }



}
